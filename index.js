const TELEGRAM_TOKEN = '6184152631:AAGsNlYmu3mpkcQUJWAeLLJ97y9XlCOrtEU';
import express from 'express';
import axios from 'axios';

const TELEGRAM_API = `https://api.telegram.org/bot${TELEGRAM_TOKEN}`
const URI = `/webhook/${TELEGRAM_TOKEN}`
const webhookURL = `https://sentry-telegram-bot.ariadne-chat.online${URI}`
const url = 'https://sentry-telegram-bot.ariadne-chat.online/webhook/6184152631:AAGsNlYmu3mpkcQUJWAeLLJ97y9XlCOrtEU'
const app = express();

app.use(express.json())

const setupWebhook = async () => {
  try {
    await axios.get(`${TELEGRAM_API}/setWebhook?url=${webhookURL}&drop_pending_updates=true`)
  } catch (error) {
    return error
  }
}

app.post(URI, async (req, res) => {
  try {
    const { message, chatId } = req.body;
    console.log(req.body);
    await axios.post(`https://api.telegram.org/bot${TELEGRAM_TOKEN}/sendMessage?chat_id=${chatId}&text=${message}`);
    res.status(200).send('ok')
  } catch (error) {
    console.log(error.message);
    res.status(500).send(error.message);
  }
})

app.listen(8001, async () => {
  try {
    console.log(`Server is up and Running at PORT : 8001`)
    await setupWebhook()
  } catch (error) {
    console.log(error.message)
  }
})